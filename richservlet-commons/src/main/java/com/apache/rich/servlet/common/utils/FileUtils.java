package com.apache.rich.servlet.common.utils;

import com.apache.rich.servlet.common.utils.IOUtils;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public abstract class FileUtils {
   public static final String EMPTY = "";
   public static final long ONE_KB = 1024L;
   public static final long ONE_MB = 1048576L;
   public static final long ONE_GB = 1073741824L;
   public static final long ONE_TB = 1099511627776L;
   public static final long ONE_PB = 1125899906842624L;
   public static final long ONE_EB = 1152921504606846976L;
   private static final long FILE_COPY_BUFFER_SIZE = 31457280L;

   public static String getExtension(String fileName) {
      if(null == fileName) {
         return "";
      } else {
         int pointIndex = fileName.lastIndexOf(".");
         return pointIndex > 0 && pointIndex < fileName.length()?fileName.substring(pointIndex + 1).toLowerCase():"";
      }
   }

   public static void writeByteArrayToFile(File file, byte[] data, boolean append) throws IOException {
      FileOutputStream out = null;

      try {
         out = openOutputStream(file, append);
         out.write(data);
      } finally {
         IOUtils.closeQuietly(out);
      }

   }

   public static byte[] readFileToByteArray(File file) throws IOException {
      FileInputStream in = null;

      byte[] arg1;
      try {
         in = openInputStream(file);
         arg1 = IOUtils.toByteArray(in, file.length());
      } finally {
         IOUtils.closeQuietly(in);
      }

      return arg1;
   }

   public static void writeStringToFile(File file, String data, String encoding, boolean append) throws IOException {
      FileOutputStream out = null;

      try {
         out = openOutputStream(file, append);
         IOUtils.write(data, out, encoding);
      } finally {
         IOUtils.closeQuietly(out);
      }

   }

   public static String readFileToString(File file, String encoding) throws IOException {
      FileInputStream in = null;

      String arg2;
      try {
         in = openInputStream(file);
         arg2 = IOUtils.toString(in, encoding);
      } finally {
         IOUtils.closeQuietly(in);
      }

      return arg2;
   }

   public static FileOutputStream openOutputStream(File file, boolean append) throws IOException {
      if(file.exists()) {
         if(file.isDirectory()) {
            throw new IOException("File \'" + file + "\' exists but is a directory");
         }

         if(!file.canWrite()) {
            throw new IOException("File \'" + file + "\' cannot be written to");
         }
      } else {
         File parent = file.getParentFile();
         if(parent != null && !parent.mkdirs() && !parent.isDirectory()) {
            throw new IOException("Directory \'" + parent + "\' could not be created");
         }
      }

      return new FileOutputStream(file, append);
   }

   public static FileInputStream openInputStream(File file) throws IOException {
      if(file.exists()) {
         if(file.isDirectory()) {
            throw new IOException("File \'" + file + "\' exists but is a directory");
         } else if(!file.canRead()) {
            throw new IOException("File \'" + file + "\' cannot be read");
         } else {
            return new FileInputStream(file);
         }
      } else {
         throw new FileNotFoundException("File \'" + file + "\' does not exist");
      }
   }

   public static String byteCountToDisplaySize(long size) {
      String displaySize;
      if(size / 1152921504606846976L > 0L) {
         displaySize = size / 1152921504606846976L + " EB";
      } else if(size / 1152921504606846976L > 0L) {
         displaySize = size / 1152921504606846976L + " PB";
      } else if(size / 1099511627776L > 0L) {
         displaySize = size / 1099511627776L + " TB";
      } else if(size / 1073741824L > 0L) {
         displaySize = size / 1073741824L + " GB";
      } else if(size / 1048576L > 0L) {
         displaySize = size / 1048576L + " MB";
      } else if(size / 1024L > 0L) {
         displaySize = size / 1024L + " KB";
      } else {
         displaySize = size + " bytes";
      }

      return displaySize;
   }

   public static void moveDirectoryToDirectory(File src, File destDir, boolean isCover) throws IOException {
      if(src == null) {
         throw new NullPointerException("Source must not be null");
      } else if(destDir == null) {
         throw new NullPointerException("Destination directory must not be null");
      } else {
         if(destDir.exists()) {
            if(!isCover) {
               throw new IOException("Destination directory is exists and isCover=false");
            }

            deleteFileOrDirectoryQuietly(destDir.getPath());
         }

         destDir.mkdirs();
         if(!destDir.exists()) {
            throw new FileNotFoundException("Destination directory \'" + destDir + "\' create failed]");
         } else if(!destDir.isDirectory()) {
            throw new IOException("Destination \'" + destDir + "\' is not a directory");
         } else {
            boolean rename = src.renameTo(destDir);
            if(!rename) {
               if(destDir.getCanonicalPath().startsWith(src.getCanonicalPath())) {
                  throw new IOException("Cannot move directory: " + src + " to a subdirectory of itself: " + destDir);
               }

               copyDirectoryToDirectory(src, destDir);
               deleteFileOrDirectoryQuietly(src.getPath());
               if(src.exists()) {
                  throw new IOException("Failed to delete original directory \'" + src + "\' after copy to \'" + destDir + "\'");
               }
            }

         }
      }
   }

   public static void moveFileToDirectory(File srcFile, File destDir, boolean isCover) throws IOException {
      if(srcFile == null) {
         throw new NullPointerException("Source must not be null");
      } else if(destDir == null) {
         throw new NullPointerException("Destination directory must not be null");
      } else {
         if(!destDir.exists()) {
            destDir.mkdirs();
         }

         if(!destDir.exists()) {
            throw new FileNotFoundException("Destination directory \'" + destDir + "\' create failed]");
         } else if(!destDir.isDirectory()) {
            throw new IOException("Destination \'" + destDir + "\' is not a directory");
         } else {
            moveFile(srcFile, new File(destDir, srcFile.getName()), isCover);
         }
      }
   }

   public static void moveFile(File srcFile, File destFile, boolean isCover) throws IOException {
      if(null == srcFile) {
         throw new NullPointerException("Source must not be null");
      } else if(null == destFile) {
         throw new NullPointerException("Destination must not be null");
      } else if(!srcFile.exists()) {
         throw new FileNotFoundException("Source \'" + srcFile + "\' does not exist");
      } else if(srcFile.isDirectory()) {
         throw new IOException("Source \'" + srcFile + "\' is a directory");
      } else if(destFile.isDirectory()) {
         throw new IOException("Destination \'" + destFile + "\' is a directory");
      } else {
         if(destFile.exists()) {
            if(!isCover) {
               throw new IOException("Destination directory is exists and isCover=false");
            }

            deleteFileOrDirectoryQuietly(destFile.getPath());
         }

         boolean rename = srcFile.renameTo(destFile);
         if(!rename) {
            copyFile(srcFile, destFile);
            if(!srcFile.delete()) {
               deleteFileOrDirectoryQuietly(destFile.getPath());
               throw new IOException("Failed to delete original file \'" + srcFile + "\' after copy to \'" + destFile + "\'");
            }
         }

      }
   }

   public static void copyDirectoryToDirectory(File srcDir, File destDir) throws IOException {
      copyDirectoryToDirectory(srcDir, new File(destDir, srcDir.getName()), (FileFilter)null);
   }

   public static void copyDirectoryToDirectory(File srcDir, File destDir, FileFilter filter) throws IOException {
      if(srcDir == null) {
         throw new NullPointerException("Source must not be null");
      } else if(destDir == null) {
         throw new NullPointerException("Destination must not be null");
      } else if(srcDir.exists() && !srcDir.isDirectory()) {
         throw new IllegalArgumentException("Source \'" + destDir + "\' is not a directory");
      } else if(destDir.exists() && !destDir.isDirectory()) {
         throw new IllegalArgumentException("Destination \'" + destDir + "\' is not a directory");
      } else if(!srcDir.exists()) {
         throw new FileNotFoundException("Source \'" + srcDir + "\' does not exist");
      } else if(srcDir.getCanonicalPath().equals(destDir.getCanonicalPath())) {
         throw new IOException("Source \'" + srcDir + "\' and destination \'" + destDir + "\' are the same");
      } else {
         ArrayList exclusionList = null;
         if(destDir.getCanonicalPath().startsWith(srcDir.getCanonicalPath())) {
            File[] srcFiles = null == filter?srcDir.listFiles():srcDir.listFiles(filter);
            if(srcFiles != null && srcFiles.length > 0) {
               exclusionList = new ArrayList(srcFiles.length);
               File[] arg4 = srcFiles;
               int arg5 = srcFiles.length;

               for(int arg6 = 0; arg6 < arg5; ++arg6) {
                  File srcFile = arg4[arg6];
                  File copiedFile = new File(destDir, srcFile.getName());
                  exclusionList.add(copiedFile.getCanonicalPath());
               }
            }
         }

         doCopyDirectory(srcDir, destDir, filter, true, exclusionList);
      }
   }

   private static void doCopyDirectory(File srcDir, File destDir, FileFilter filter, boolean preserveFileDate, List<String> exclusionList) throws IOException {
      File[] srcFiles = null == filter?srcDir.listFiles():srcDir.listFiles(filter);
      if(srcFiles == null) {
         throw new IOException("Failed to list contents of " + srcDir);
      } else {
         if(destDir.exists()) {
            if(!destDir.isDirectory()) {
               throw new IOException("Destination \'" + destDir + "\' exists but is not a directory");
            }
         } else if(!destDir.mkdirs() && !destDir.isDirectory()) {
            throw new IOException("Destination \'" + destDir + "\' directory cannot be created");
         }

         if(!destDir.canWrite()) {
            throw new IOException("Destination \'" + destDir + "\' cannot be written to");
         } else {
            File[] arg5 = srcFiles;
            int arg6 = srcFiles.length;

            for(int arg7 = 0; arg7 < arg6; ++arg7) {
               File srcFile = arg5[arg7];
               File dstFile = new File(destDir, srcFile.getName());
               if(exclusionList == null || !exclusionList.contains(srcFile.getCanonicalPath())) {
                  if(srcFile.isDirectory()) {
                     doCopyDirectory(srcFile, dstFile, filter, preserveFileDate, exclusionList);
                  } else {
                     doCopyFile(srcFile, dstFile, preserveFileDate);
                  }
               }
            }

            if(preserveFileDate) {
               destDir.setLastModified(srcDir.lastModified());
            }

         }
      }
   }

   public static void copyFileToDirectory(File srcFile, File destDir) throws IOException {
      copyFileToDirectory(srcFile, destDir, true);
   }

   public static void copyFileToDirectory(File srcFile, File destDir, boolean preserveFileDate) throws IOException {
      if(null == destDir) {
         throw new NullPointerException("Destination must not be null");
      } else if(destDir.exists() && !destDir.isDirectory()) {
         throw new IllegalArgumentException("Destination \'" + destDir + "\' is not a directory");
      } else {
         File destFile = new File(destDir, srcFile.getName());
         copyFile(srcFile, destFile, preserveFileDate);
      }
   }

   public static void copyFile(File srcFile, File destFile) throws IOException {
      copyFile(srcFile, destFile, true);
   }

   public static void copyFile(File srcFile, File destFile, boolean preserveFileDate) throws IOException {
      if(srcFile == null) {
         throw new NullPointerException("Source must not be null");
      } else if(destFile == null) {
         throw new NullPointerException("Destination must not be null");
      } else if(!srcFile.exists()) {
         throw new FileNotFoundException("Source \'" + srcFile + "\' does not exist");
      } else if(destFile.isDirectory()) {
         throw new IOException("Destination \'" + destFile + "\' exists but is a directory");
      } else if(srcFile.getCanonicalPath().equals(destFile.getCanonicalPath())) {
         throw new IOException("Source \'" + srcFile + "\' and destination \'" + destFile + "\' are the same");
      } else if(destFile.exists() && !destFile.canWrite()) {
         throw new IOException("Destination \'" + destFile + "\' exists but is read-only");
      } else {
         File parentFile = destFile.getParentFile();
         if(null != parentFile && !parentFile.mkdirs() && !parentFile.isDirectory()) {
            throw new IOException("Destination \'" + parentFile + "\' directory cannot be created");
         } else {
            doCopyFile(srcFile, destFile, preserveFileDate);
         }
      }
   }

   private static void doCopyFile(File srcFile, File destFile, boolean preserveFileDate) throws IOException {
      FileInputStream fis = null;
      FileOutputStream fos = null;
      FileChannel input = null;
      FileChannel output = null;

      try {
         fis = new FileInputStream(srcFile);
         fos = new FileOutputStream(destFile);
         input = fis.getChannel();
         output = fos.getChannel();
         long size = input.size();
         long pos = 0L;

         for(long count = 0L; pos < size; pos += output.transferFrom(input, pos, count)) {
            count = size - pos > 31457280L?31457280L:size - pos;
         }
      } finally {
         IOUtils.closeQuietly(output);
         IOUtils.closeQuietly(fos);
         IOUtils.closeQuietly(input);
         IOUtils.closeQuietly(fis);
      }

      if(srcFile.length() != destFile.length()) {
         throw new IOException("Failed to copy full contents from \'" + srcFile + "\' to \'" + destFile + "\'");
      } else {
         if(preserveFileDate) {
            destFile.setLastModified(srcFile.lastModified());
         }

      }
   }

   public static void deleteFileOrDirectoryQuietly(String filePath) {
      try {
         deleteFileOrDirectory(filePath, true);
      } catch (Exception arg1) {
         ;
      }

   }

   public static void deleteFileOrDirectory(String filePath, boolean deleteThisPath) throws IOException {
      if(null != filePath && filePath.length() > 0) {
         File file = new File(filePath);
         if(file.isDirectory()) {
            File[] files = file.listFiles();

            for(int i = 0; i < files.length; ++i) {
               deleteFileOrDirectory(files[i].getAbsolutePath(), true);
            }
         }

         if(deleteThisPath) {
            if(!file.isDirectory()) {
               file.delete();
            } else if(file.listFiles().length == 0) {
               file.delete();
            }
         }
      }

   }

//   public static List<File> getFileListByPath(String path) {
//      return getFileListByPath(path, (FileFilter)null);
//   }

//   public static List<File> getFileListByPath(String path, FileFilter filter) {
//      File directory = new File(path);
//      if(directory.exists() && directory.isDirectory()) {
//         return (new Recursiver((1))).getFileList(directory, filter);
//      } else {
//         throw new IllegalArgumentException("Nonexistent directory[" + path + "]");
//      }
//   }

   public static Properties readProperties(String path) {
      File file = new File(path);
      if(!file.exists()) {
         return null;
      } else {
         Properties properties = new Properties();
         FileInputStream in = null;

         try {
            in = new FileInputStream(file);
            properties.load(in);
         } catch (IOException arg7) {
            throw new RuntimeException("Could not read Properties[" + path + "]", arg7);
         } finally {
            IOUtils.closeQuietly(in);
         }

         return properties;
      }
   }

   public static Properties readProperties(InputStream in) {
      Properties properties = new Properties();

      try {
         properties.load(in);
      } catch (IOException arg5) {
         throw new RuntimeException("Could not read Properties", arg5);
      } finally {
         IOUtils.closeQuietly(in);
      }

      return properties;
   }

   public static void writeProperties(String path, Properties properties) {
      FileOutputStream out = null;

      try {
         out = new FileOutputStream(path);
         properties.store(out, (String)null);
      } catch (IOException arg6) {
         throw new RuntimeException("Could not write Properties[" + path + "]", arg6);
      } finally {
         IOUtils.closeQuietly(out);
      }

   }
}