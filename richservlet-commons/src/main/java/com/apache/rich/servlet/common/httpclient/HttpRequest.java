package com.apache.rich.servlet.common.httpclient;

import com.apache.rich.servlet.common.http.listener.HttpDownloadListener;
import java.io.File;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

public class HttpRequest {
	private String mUrl;
	private Map<String, String> mParamMap;
	private Map<String, File> mFileParamMap;
	private Map<String, String> mHeaderMap;
	private String mBodyJson;
	private String mEncode = "utf-8";
	private int mConnectionTimeout = 30000;
	private int mReadTimeout = 30000;
	private HttpDownloadListener mDownloadListener;
	private String mDownloadFileName;

	public HttpRequest() {
		this.init();
	}

	public int getConnectionTimeout() {
		return this.mConnectionTimeout;
	}

	public void setConnectionTimeout(int connectionTimeout) {
		this.mConnectionTimeout = connectionTimeout;
	}

	public String getDownloadFileName() {
		return this.mDownloadFileName;
	}

	public void setDownloadFileName(String downloadFileName) {
		this.mDownloadFileName = downloadFileName;
	}

	public String getEncode() {
		return this.mEncode;
	}

	public void setEncode(String encode) {
		this.mEncode = encode;
	}

	public int getReadTimeout() {
		return this.mReadTimeout;
	}

	public void setReadTimeout(int readTimeout) {
		this.mReadTimeout = readTimeout;
	}

	public HttpDownloadListener getDownloadListener() {
		return this.mDownloadListener;
	}

	public void setDownloadListener(HttpDownloadListener downloadListener) {
		this.mDownloadListener = downloadListener;
	}

	public String getUrl() {
		return this.mUrl;
	}

	public void setUrl(String url) {
		this.mUrl = url;
	}

	public void putBodyJson(String bodyJson) {
		if (null != bodyJson) {
			this.mBodyJson = bodyJson;
		}

	}

	public void putParam(String key, String value) {
		if (key != null && value != null) {
			this.mParamMap.put(key, value);
		}

	}

	public void putFile(String key, File file) {
		this.mFileParamMap.put(key, file);
	}

	public void putHeader(String key, String value) {
		this.mHeaderMap.put(key, value);
	}

	public void removeParam(String key) {
		this.mParamMap.remove(key);
	}

	public void removeFile(String key) {
		this.mFileParamMap.remove(key);
	}

	public void removeHeader(String key) {
		this.mHeaderMap.remove(key);
	}

	public String getParamsStr() {
		StringBuilder sb = new StringBuilder();
		Iterator arg1 = this.mParamMap.entrySet().iterator();

		while (arg1.hasNext()) {
			Entry entry = (Entry) arg1.next();
			sb.append((String) entry.getKey()).append("=")
					.append(URLEncoder.encode((String) entry.getValue()))
					.append("&");
		}

		if (sb.length() > 0) {
			sb.deleteCharAt(sb.length() - 1);
		}

		return sb.toString();
	}

	public String getGetUrl() {
		return !this.getUrl().contains("?") ? this.getUrl() + "?"
				+ this.getParamsStr() : this.getUrl() + "&"
				+ this.getParamsStr();
	}

	public Map<String, String> getParamMap() {
		return this.mParamMap;
	}

	public Map<String, File> getFileParamMap() {
		return this.mFileParamMap;
	}

	public Map<String, String> getHeaderMap() {
		return this.mHeaderMap;
	}

	public String getBodyJson() {
		return this.mBodyJson;
	}

	private void init() {
		this.mParamMap = new ConcurrentHashMap();
		this.mFileParamMap = new ConcurrentHashMap();
		this.mHeaderMap = new ConcurrentHashMap();
	}

	public String toString() {
		StringBuilder result = new StringBuilder();
		Iterator arg1 = this.mParamMap.entrySet().iterator();

		Entry entry;
		while (arg1.hasNext()) {
			entry = (Entry) arg1.next();
			if (result.length() > 0) {
				result.append("&");
			}

			result.append((String) entry.getKey());
			result.append("=");
			result.append((String) entry.getValue());
		}

		arg1 = this.mFileParamMap.entrySet().iterator();

		while (arg1.hasNext()) {
			entry = (Entry) arg1.next();
			if (result.length() > 0) {
				result.append("&");
			}

			result.append((String) entry.getKey());
			result.append("=");
			result.append("FILE");
		}

		if (!this.getUrl().contains("?")) {
			return this.getUrl() + "?" + result.toString();
		} else {
			return this.getUrl() + "&" + result.toString();
		}
	}
}