package com.apache.rich.servlet.common.enums;

public enum ErrorCodeEnums {
	SYSTEM_ERROR("SYSTEM_ERROR", "系统异常"), PARAMETER_ERROR("PARAMETER_ERROR",
			"参数异常"), SCAN_PACKGE_ERROR("SCAN_PACKGE_ERROR", "扫描包异常");

	private String errorCode;
	private String errorMessage;

	private ErrorCodeEnums(String errorCode, String errorMessage) {
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}

	public String getErrorCode() {
		return this.errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return this.errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
}