package com.apache.rich.servlet.common.http.listener;

public interface HttpDownloadListener {
	void callBack(long arg0, long arg2, boolean arg4);
}