
package com.apache.rich.servlet.http.servlet.realize;

import io.netty.buffer.ByteBufInputStream;
import io.netty.buffer.Unpooled;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpRequest;

import javax.servlet.ServletInputStream;
import java.io.IOException;

/**
 * 
 * @author wanghailing
 *
 */
public class RichServletHttpServletInputStream extends ServletInputStream {

    private HttpRequest request;

    private ByteBufInputStream in;

    public RichServletHttpServletInputStream(FullHttpRequest request) {
        this.request = request;

        this.in = new ByteBufInputStream(request.content());
    }

    public RichServletHttpServletInputStream(HttpRequest request) {
        this.request = request;

        this.in = new ByteBufInputStream(Unpooled.buffer(0));
    }


    @Override
    public int read() throws IOException {
        return this.in.read();
    }

    @Override
    public int read(byte[] buf) throws IOException {
        return this.in.read(buf);
    }

    @Override
    public int read(byte[] buf, int offset, int len) throws IOException {
        return this.in.read(buf, offset, len);
    }

}
