
package com.apache.rich.servlet.http.servlet.realize.session;

/**
 * 
 * @author wanghailing
 *
 */
public interface RichServletHttpSessionStore {

	RichServletHttpSession findSession(String sessionId);

	RichServletHttpSession createSession();

    void destroySession(String sessionId);

    void destroyInactiveSessions();

}
