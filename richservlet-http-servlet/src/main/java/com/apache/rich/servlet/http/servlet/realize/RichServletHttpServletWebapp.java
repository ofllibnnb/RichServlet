
package com.apache.rich.servlet.http.servlet.realize;

import io.netty.channel.group.ChannelGroup;

import java.io.File;
import java.util.Map;

import com.apache.rich.servlet.http.servlet.realize.configuration.RichServletHttpFilterConfiguration;

import com.apache.rich.servlet.http.servlet.realize.configuration.RichServletHttpServletContextListenerConfiguration;
import com.apache.rich.servlet.http.servlet.realize.configuration.RichServletHttpServletConfiguration;
import com.apache.rich.servlet.http.servlet.realize.configuration.RichServletHttpWebappConfiguration;

/**
 * 
 * @author wanghailing
 *
 */
public class RichServletHttpServletWebapp {

    private static RichServletHttpServletWebapp instance;

    private RichServletHttpWebappConfiguration webappConfig;

    private ChannelGroup sharedChannelGroup;

    public static RichServletHttpServletWebapp get() {

        if (instance == null)
            instance = new RichServletHttpServletWebapp();

        return instance;
    }

    private RichServletHttpServletWebapp() {
    }

    public void init(RichServletHttpWebappConfiguration webapp, ChannelGroup sharedChannelGroup) {
        this.webappConfig = webapp;
        this.sharedChannelGroup = sharedChannelGroup;
        this.initServletContext();
        this.initContextListeners();
        this.initFilters();
        this.initServlets();
    }

    public void destroy() {
        this.destroyServlets();
        this.destroyFilters();
        this.destroyContextListeners();
    }

    private void initContextListeners() {
        if (webappConfig.getServletContextListenerConfigurations() != null) {
            for (RichServletHttpServletContextListenerConfiguration ctx : webappConfig
                    .getServletContextListenerConfigurations()) {
                ctx.init();
            }
        }
    }

    private void destroyContextListeners() {
        if (webappConfig.getServletContextListenerConfigurations() != null) {
            for (RichServletHttpServletContextListenerConfiguration ctx : webappConfig
                    .getServletContextListenerConfigurations()) {
                ctx.destroy();
            }
        }
    }

    private void destroyServlets() {
        if (webappConfig.getServletConfigurations() != null) {
            for (RichServletHttpServletConfiguration servlet : webappConfig
                    .getServletConfigurations()) {
                servlet.destroy();
            }
        }
    }

    private void destroyFilters() {
        if (webappConfig.getFilterConfigurations() != null) {
            for (RichServletHttpFilterConfiguration filter : webappConfig
                    .getFilterConfigurations()) {
                filter.destroy();
            }
        }
    }

    protected void initServletContext() {
    		RichServletHttpServletContext ctx = RichServletHttpServletContext.get();
        ctx.setServletContextName(this.webappConfig.getName());
        if (webappConfig.getContextParameters() != null) {
            for (Map.Entry<String, String> entry : webappConfig
                    .getContextParameters().entrySet()) {
                ctx.addInitParameter(entry.getKey(), entry.getValue());
            }
        }
    }

    protected void initFilters() {
        if (webappConfig.getFilterConfigurations() != null) {
            for (RichServletHttpFilterConfiguration filter : webappConfig
                    .getFilterConfigurations()) {
                filter.init();
            }
        }
    }

    protected void initServlets() {
        if (webappConfig.hasServletConfigurations()) {
            for (RichServletHttpServletConfiguration servlet : webappConfig
                    .getServletConfigurations()) {
                servlet.init();
            }
        }
    }

    public RichservletHttpFilterChain initializeChain(String uri) {
    		RichServletHttpServletConfiguration servletConfiguration = this.findServlet(uri);
    		RichservletHttpFilterChain chain = new RichservletHttpFilterChain(servletConfiguration);

        if (this.webappConfig.hasFilterConfigurations()) {
            for (RichServletHttpFilterConfiguration s : this.webappConfig
                    .getFilterConfigurations()) {
                if (s.matchesUrlPattern(uri))
                    chain.addFilterConfiguration(s);
            }
        }

        return chain;
    }

    private RichServletHttpServletConfiguration findServlet(String uri) {

        if (!this.webappConfig.hasServletConfigurations()) {
            return null;
        }

        for (RichServletHttpServletConfiguration s : this.webappConfig
                .getServletConfigurations()) {
            if (s.matchesUrlPattern(uri))
                return s;
        }

        return null;
    }

    public File getStaticResourcesFolder() {
        return this.webappConfig.getStaticResourcesFolder();
    }

    public RichServletHttpWebappConfiguration getWebappConfig() {
        return webappConfig;
    }

    public ChannelGroup getSharedChannelGroup() {
        return sharedChannelGroup;
    }
}
