package com.apache.rich.servlet.http.servlet.realize.adapter;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import com.apache.rich.servlet.http.servlet.realize.utils.HttpServletUtils;

/**
 * http servlet config 适配器
 * @author wanghailing
 *
 */
public abstract class ConfigAdapter {
	
	private String ownerName;

    private Map<String, String> initParameters;

    public ConfigAdapter(String ownerName) {
        this.ownerName = ownerName;
    }

    public void addInitParameter(String name, String value) {
        if (this.initParameters == null)
            this.initParameters = new HashMap<String, String>();

        this.initParameters.put(name, value);
    }

    public String getInitParameter(String name) {
        if (this.initParameters == null)
            return null;

        return this.initParameters.get(name);
    }

    @SuppressWarnings("rawtypes")
	public Enumeration getInitParameterNames() {
        return HttpServletUtils.enumerationFromKeys(this.initParameters);
    }

    protected String getOwnerName() {
        return ownerName;
    }
}
