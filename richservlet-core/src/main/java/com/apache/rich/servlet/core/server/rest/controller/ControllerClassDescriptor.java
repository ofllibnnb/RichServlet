package com.apache.rich.servlet.core.server.rest.controller;

/**
 * controller class
 *
 * Author : wanghailing
 */
public class ControllerClassDescriptor {

    private Class<?> clazz;

    public ControllerClassDescriptor(Class<?> clazz) {
        this.clazz = clazz;
    }

    public Class<?> getClazz() {
        return clazz;
    }
}
