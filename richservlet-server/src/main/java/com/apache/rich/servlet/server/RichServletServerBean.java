/**
 * 
 */
package com.apache.rich.servlet.server;

import java.io.Serializable;
import java.util.List;

/**
 * @author wanghailing
 *
 */
public class RichServletServerBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7903517825896671299L;
	/**
	 * io处理的线程数
	 */
	private Integer ioThreads;
	
	/**
	 * 工作线程数
	 */
	private Integer workThreads;
	
	/**
	 * ip地址
	 */
	private String serverHost;
	
	/**
	 * 端口号
	 */
	private Integer serverPort;
	
	/**
	 * 最大连接数
	 */
	private Integer maxConnections;
	
	/**
	 * 支持的方式
	 * 1: http
	 * 2: https
	 * 3: http servlet
	 */
	private String supportType;
	
	/**
	 * 
	 */
	private String webpatch;
	
	/**
	 * HttpServlet
	 */
	private List<HttpServletBean> httpServletBeans;
	
	/**
	 * 
	 */
	private List<HttpFilterBean> httpFilterBeans;
	/**
	 * 扫描控制层路径
	 */
	private String scanControllerPatch;
	
	/**
	 * 
	 */
	private Integer tcpBacklogs;
	/**
	 * @return the ioThreads
	 */
	public Integer getIoThreads() {
		return ioThreads;
	}

	/**
	 * @param ioThreads the ioThreads to set
	 */
	public void setIoThreads(Integer ioThreads) {
		this.ioThreads = ioThreads;
	}

	/**
	 * @return the workThreads
	 */
	public Integer getWorkThreads() {
		return workThreads;
	}

	/**
	 * @param workThreads the workThreads to set
	 */
	public void setWorkThreads(Integer workThreads) {
		this.workThreads = workThreads;
	}

	/**
	 * @return the serverHost
	 */
	public String getServerHost() {
		return serverHost;
	}

	/**
	 * @param serverHost the serverHost to set
	 */
	public void setServerHost(String serverHost) {
		this.serverHost = serverHost;
	}

	/**
	 * @return the serverPort
	 */
	public Integer getServerPort() {
		return serverPort;
	}

	/**
	 * @param serverPort the serverPort to set
	 */
	public void setServerPort(Integer serverPort) {
		this.serverPort = serverPort;
	}

	/**
	 * @return the maxConnections
	 */
	public Integer getMaxConnections() {
		return maxConnections;
	}

	/**
	 * @param maxConnections the maxConnections to set
	 */
	public void setMaxConnections(Integer maxConnections) {
		this.maxConnections = maxConnections;
	}

	/**
	 * @return the supportType
	 */
	public String getSupportType() {
		return supportType;
	}

	/**
	 * @param supportType the supportType to set
	 */
	public void setSupportType(String supportType) {
		this.supportType = supportType;
	}
	
	
	/**
	 * @return the webpatch
	 */
	public String getWebpatch() {
		return webpatch;
	}

	/**
	 * @param webpatch the webpatch to set
	 */
	public void setWebpatch(String webpatch) {
		this.webpatch = webpatch;
	}

	/**
	 * @return the httpServletBeans
	 */
	public List<HttpServletBean> getHttpServletBeans() {
		return httpServletBeans;
	}

	/**
	 * @param httpServletBeans the httpServletBeans to set
	 */
	public void setHttpServletBeans(List<HttpServletBean> httpServletBeans) {
		this.httpServletBeans = httpServletBeans;
	}

	/**
	 * @return the scanControllerPatch
	 */
	public String getScanControllerPatch() {
		return scanControllerPatch;
	}

	/**
	 * @param scanControllerPatch the scanControllerPatch to set
	 */
	public void setScanControllerPatch(String scanControllerPatch) {
		this.scanControllerPatch = scanControllerPatch;
	}

	/**
	 * @return the tcpBacklogs
	 */
	public Integer getTcpBacklogs() {
		return tcpBacklogs;
	}

	/**
	 * @param tcpBacklogs the tcpBacklogs to set
	 */
	public void setTcpBacklogs(Integer tcpBacklogs) {
		this.tcpBacklogs = tcpBacklogs;
	}

	/**
	 * @return the httpFilterBeans
	 */
	public List<HttpFilterBean> getHttpFilterBeans() {
		return httpFilterBeans;
	}

	/**
	 * @param httpFilterBeans the httpFilterBeans to set
	 */
	public void setHttpFilterBeans(List<HttpFilterBean> httpFilterBeans) {
		this.httpFilterBeans = httpFilterBeans;
	}
	
	
}
